# programa del salario usando try y except.
# autor: Danilo Delgado
# email: edwin.delgado@unl.edu.ec
try:
    hr = int(input("ingrese el numero de horas trabajadas:  "))

    tf = float(input("ingrese el valor por hora trabajada: "))

    if hr > 40:
        hrt = hr - 40
        vex = (hrt * 1.5) * tf
        vb = (40 * tf) + vex
        print("el valor del salario es: ", vb)
    else:
        vb = hr * tf
        print("el valor del salario es: ", vb)
except:
    print("Error, por favor ingrese un número")

