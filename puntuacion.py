#calificaciones cuantitativas
#Autor: Danilo Delgado
#Email: edwin.delgado@unl.edu.ec
try:
    print("la puntuacion debe ir entre 0.0 y 1.0")
    cal= float (input ("Ingrese puntuación: "))

    #validacion
    if  cal >=0 and cal <=1:
        if cal >= 0.9:
            print("Sobresaliente")
        elif cal >=0.8:
            print("notable")
        elif cal >= 0.7:
          print("bueno")
        elif cal >= 0.6:
            print("Suficiente")
        elif cal < 0.6:
            print("Insuficiente")
    else:
        print("La puntuacion es incorrecta")
except:
    print("La puntuacion es incorrecta")
