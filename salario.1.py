# programa del cálculo del salario para darle al empleado 1.5 veces la
# tarifa horaria para todas las horas trabajadas que excedan de 40.
# autor: Danilo Delgado
# email: edwin.delgado@unl.edu.ec
hr = int(input("ingrese el número de horas trabajadas:  "))

tf = float(input("ingrese el valor por hora trabajada: "))

if hr > 40:
    hrt = hr - 40
    vex = (hrt * 1.5) * tf
    vb = (40 * tf) + vex
    print("el valor del salario es: ", vb)
else:
    vb = hr * tf
    print("el valor del salario es: ", vb)